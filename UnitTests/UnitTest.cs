﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Service.StringToMath;


namespace UnitTests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void AddSubtractTest()
        {
            var stringToMathService = new StringToMathService();

            // Add 
            Assert.AreEqual(stringToMathService.Eval("10 + 20"), 30);

            // Subtract 
            Assert.AreEqual(stringToMathService.Eval("10 - 20"), -10);

            // Division
            Assert.AreEqual(stringToMathService.Eval("10 / 2"), 5);

            // Mul
            Assert.AreEqual(stringToMathService.Eval("2 * 2"), 4);

            // Sequence
            Assert.AreEqual(stringToMathService.Eval("2 * 2 / 2 + 3 - 1"), 4);
        }


        [TestMethod]
        public void OrderOfOperation()
        {
            var stringToMathService = new StringToMathService();

            // No parens
            Assert.AreEqual(stringToMathService.Eval("10 + 20 * 30"), 610);

            // Parens
            Assert.AreEqual(stringToMathService.Eval("(10 + 20) * 30"), 900);

        }


    }
}

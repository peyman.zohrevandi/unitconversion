﻿using System.Collections.Generic;

namespace Service.BaseUnit
{
    public interface IBaseUnitService
    {
        IEnumerable<Core.Domain.BaseUnit> GetAll();

        IList<Core.Domain.BaseUnit> GetByIds(int[] ids);

        Core.Domain.BaseUnit GetById(int id);

        void Insert(Core.Domain.BaseUnit baseUnit);
    }
}

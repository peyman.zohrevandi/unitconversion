﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Data;

namespace Service.BaseUnit
{
    public class BaseUnitService : IBaseUnitService
    {
        private readonly IRepository<Core.Domain.BaseUnit> _baseUnitRepository;

        public BaseUnitService(IRepository<Core.Domain.BaseUnit> baseUnitRepository)
        {
            _baseUnitRepository = baseUnitRepository;
        }

        public IEnumerable<Core.Domain.BaseUnit> GetAll()
        {
            var res = _baseUnitRepository.Include(x => x.MeasureDimension);
            return res;
        }

        public IList<Core.Domain.BaseUnit> GetByIds(int[] ids)
        {
            var query = _baseUnitRepository.Table;
            return query.Where(p => ids.Contains(p.Id)).ToList();
        }

        public Core.Domain.BaseUnit GetById(int id)
        {
            if (id <= 0)
                return null;

            return _baseUnitRepository.GetById(id);
        }

        public void Insert(Core.Domain.BaseUnit baseUnit)
        {
            if (baseUnit == null)
                throw new ArgumentNullException(nameof(baseUnit));

            _baseUnitRepository.Insert(baseUnit);
        }
    }
}

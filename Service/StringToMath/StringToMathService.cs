﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service.StringToMath
{
    public class StringToMathService : IStringToMathService
    {
        private readonly string[] _operators = { "-", "+", "/", "*" };

        private readonly Func<double, double, double>[] _operations =
        {
            (a1, a2) => a1 - a2,
            (a1, a2) => a1 + a2,
            (a1, a2) => a1 / a2,
            (a1, a2) => a1 * a2
        };

        public double Eval(string expression)
        {
            var tokens = GetTokens(expression);
            var operandStack = new Stack<double>();
            var operatorStack = new Stack<string>();
            var tokenIndex = 0;

            while (tokenIndex < tokens.Count)
            {
                var token = tokens[tokenIndex];
                switch (token)
                {
                    case "(":
                        {
                            var subExpr = GetSubExpression(tokens, ref tokenIndex);
                            operandStack.Push(Eval(subExpr));
                            continue;
                        }
                    case ")":
                        throw new ArgumentException("Mis-matched parentheses in expression");
                }

                //If this is an operator  
                if (Array.IndexOf(_operators, token) >= 0)
                {
                    while (operatorStack.Count > 0 && Array.IndexOf(_operators, token) < Array.IndexOf(_operators, operatorStack.Peek()))
                    {
                        var op = operatorStack.Pop();
                        var arg2 = operandStack.Pop();
                        var arg1 = operandStack.Pop();
                        operandStack.Push(_operations[Array.IndexOf(_operators, op)](arg1, arg2));
                    }
                    operatorStack.Push(token);
                }
                else
                {
                    operandStack.Push(double.Parse(token));
                }
                tokenIndex += 1;
            }

            while (operatorStack.Count > 0)
            {
                var op = operatorStack.Pop();
                var arg2 = operandStack.Pop();
                var arg1 = operandStack.Pop();
                operandStack.Push(_operations[Array.IndexOf(_operators, op)](arg1, arg2));
            }
            return operandStack.Pop();
        }

        public string GetSubExpression(List<string> tokens, ref int index)
        {
            var subExpr = new StringBuilder();
            var parentlevels = 1;
            index += 1;
            while (index < tokens.Count && parentlevels > 0)
            {
                var token = tokens[index];
                if (tokens[index] == "(")
                {
                    parentlevels += 1;
                }

                if (tokens[index] == ")")
                {
                    parentlevels -= 1;
                }

                if (parentlevels > 0)
                {
                    subExpr.Append(token);
                }

                index += 1;
            }

            if ((parentlevels > 0))
            {
                throw new ArgumentException("Mis-matched parentheses in expression");
            }
            return subExpr.ToString();
        }

        public List<string> GetTokens(string expression)
        {
            var operators = "()*/+-";
            var tokens = new List<string>();
            var sb = new StringBuilder();

            foreach (var c in expression.Replace(" ", string.Empty))
            {
                if (operators.IndexOf(c) >= 0)
                {
                    if ((sb.Length > 0))
                    {
                        tokens.Add(sb.ToString());
                        sb.Length = 0;
                    }
                    tokens.Add(c.ToString());
                }
                else
                {
                    sb.Append(c);
                }
            }

            if ((sb.Length > 0))
            {
                tokens.Add(sb.ToString());
            }
            return tokens;
        }
    }
}

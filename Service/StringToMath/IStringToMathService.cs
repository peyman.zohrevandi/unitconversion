﻿using System.Collections.Generic;

namespace Service.StringToMath
{
    public interface IStringToMathService
    {
        double Eval(string expression);

        string GetSubExpression(List<string> tokens, ref int index);

        List<string> GetTokens(string expression);
    }
}

﻿using System.Collections.Generic;

namespace Service.FactorUnit
{
    public interface IFactorUnitService
    {
        IEnumerable<Core.Domain.Unit> GetAll();

        IList<Core.Domain.Unit> GetByIds(int[] ids);

        Core.Domain.Unit GetById(int id);

        void Insert(Core.Domain.Unit factorUnit);
    }
}

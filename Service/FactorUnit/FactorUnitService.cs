﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Data;

namespace Service.FactorUnit
{
    public class FactorUnitService : IFactorUnitService
    {
        private readonly IRepository<Core.Domain.Unit> _factorUnitRepository;

        public FactorUnitService(IRepository<Core.Domain.Unit> factorUnitRepository)
        {
            _factorUnitRepository = factorUnitRepository;
        }

        public IEnumerable<Core.Domain.Unit> GetAll()
        {
            return _factorUnitRepository.Table;
        }

        public IList<Core.Domain.Unit> GetByIds(int[] ids)
        {
            var query = _factorUnitRepository.Table;
            return query.Where(p => ids.Contains(p.Id)).ToList();
        }

        public Core.Domain.Unit GetById(int id)
        {

            if (id <= 0)
                return null;

            return _factorUnitRepository.GetById(id);
        }

        public void Insert(Core.Domain.Unit factorUnit)
        {
            if (factorUnit == null)
                throw new ArgumentNullException(nameof(factorUnit));

            _factorUnitRepository.Insert(factorUnit);
        }
    }
}

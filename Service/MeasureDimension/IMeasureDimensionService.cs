﻿using System.Collections.Generic;

namespace Service.MeasureDimension
{
    public interface IMeasureDimensionService
    {
        IEnumerable<Core.Domain.MeasureDimension> GetAll();

        IList<Core.Domain.MeasureDimension> GetByIds(int[] ids);

        Core.Domain.MeasureDimension GetById(int id);

        void Insert(Core.Domain.MeasureDimension measureDimension);
    }
}

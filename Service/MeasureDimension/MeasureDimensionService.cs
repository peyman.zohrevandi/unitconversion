﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Data;

namespace Service.MeasureDimension
{
    public class MeasureDimensionService : IMeasureDimensionService
    {
        private readonly IRepository<Core.Domain.MeasureDimension> _measureDimensionRepository;

        public MeasureDimensionService(IRepository<Core.Domain.MeasureDimension> measureDimensionRepository)
        {
            _measureDimensionRepository = measureDimensionRepository;
        }

        public IEnumerable<Core.Domain.MeasureDimension> GetAll()
        {
            return _measureDimensionRepository.Table;
        }

        public IList<Core.Domain.MeasureDimension> GetByIds(int[] ids)
        {
            var query = _measureDimensionRepository.Table;
            return query.Where(p => ids.Contains(p.Id)).ToList();
        }

        public Core.Domain.MeasureDimension GetById(int id)
        {
            if (id <= 0)
                return null;

            return _measureDimensionRepository.GetById(id);
        }

        public void Insert(Core.Domain.MeasureDimension measureDimension)
        {
            if (measureDimension == null)
                throw new ArgumentNullException(nameof(measureDimension));

            _measureDimensionRepository.Insert(measureDimension);
        }
    }
}

﻿using System;
using System.Linq;
using Core.Dots.ConvertUnit;
using Service.BaseUnit;
using Service.FactorUnit;
using Service.StringToMath;

namespace Service.ConvertUnit
{
    public class ConvertUnitService : IConvertUnitService
    {
        private readonly IFactorUnitService _factorUnitService;
        private readonly IBaseUnitService _baseUnitService;
        private readonly IStringToMathService _stringToMathService;

        public ConvertUnitService(IFactorUnitService factorUnitService, IBaseUnitService baseUnitService, IStringToMathService stringToMathService)
        {
            _factorUnitService = factorUnitService;
            _baseUnitService = baseUnitService;
            _stringToMathService = stringToMathService;
        }

        public double ConvertUnit(ConvertUnitDto dto)
        {
            if(dto == null)
                throw new ArgumentNullException(nameof(dto));

            var fromBaseUnit = _factorUnitService.GetAll().FirstOrDefault(x => x.Symbol == dto.FromSymbol);
            var toBaseUnit = _factorUnitService.GetAll().FirstOrDefault(x => x.Symbol == dto.ToSymbol);

            var isBaseUnitFormula = _baseUnitService.GetById(dto.BaseUnitId).IsFormula;

            if (isBaseUnitFormula)
            {
                if (fromBaseUnit == null)
                {
                    if (toBaseUnit != null)
                        return _stringToMathService.Eval(
                            toBaseUnit.FromBaseUnitFormula.Replace("a", dto.Number.ToString()));
                }

                if (toBaseUnit == null)
                {
                    if (fromBaseUnit != null)
                        return _stringToMathService.Eval(
                            fromBaseUnit.ToBaseUnitFormula.Replace("a", dto.Number.ToString()));
                }
            }
           

            if (isBaseUnitFormula == false)
            {
                if (fromBaseUnit == null && toBaseUnit == null)
                {
                    return 1 * dto.Number / 1;
                }

                if (fromBaseUnit == null)
                {
                    return 1 * dto.Number / toBaseUnit.NumberOfBaseUnit;
                }
                if (toBaseUnit == null)
                {
                    return fromBaseUnit.NumberOfBaseUnit * dto.Number / 1;
                }

                return fromBaseUnit.NumberOfBaseUnit * dto.Number / toBaseUnit.NumberOfBaseUnit;
            }

            return 0;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Dots.ConvertUnit;

namespace Service.ConvertUnit
{
    public interface IConvertUnitService
    {
        double ConvertUnit(ConvertUnitDto dto);
    }
}

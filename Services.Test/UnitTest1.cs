﻿using System.Collections.Generic;
using System.Linq;
using Core.Data;
using Core.Domain;
using Moq;
using NUnit.Framework;
using Service.MeasureDimension;

namespace Services.Test
{
    [TestFixture]
    public class Tests
    {
        private Mock<IRepository<MeasureDimension>> _measureDimensionRepo;
        private IMeasureDimensionService _measureDimensionService;

        [Test]
        public void Test1()
        {
            Assert.Pass();
        }

        [SetUp]
        public void Setup()
        {
            _measureDimensionRepo = new Mock<IRepository<MeasureDimension>>();

            var measureDimension = new MeasureDimension
            {
                Id = 1,
                EnName = "طول",
                Name = "Length"
            };
            var measureDimension2 = new MeasureDimension
            {
                Id = 1,
                EnName = "طول",
                Name = "Length"
            };

            _measureDimensionRepo.Setup(x => x.Table).Returns(new List<MeasureDimension> { measureDimension, measureDimension2 }.AsQueryable());
        }

        [Test]
        public void Can_get_all_measureDimension()
        {
            var measureDimensionService = _measureDimensionService.GetAll();
            Assert.IsNotNull(measureDimensionService);
            Assert.IsTrue(measureDimensionService.Any());
        }
    }
}
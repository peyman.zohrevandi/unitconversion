﻿using Core.Data;
using Data;
using Microsoft.Extensions.DependencyInjection;
using Presentation.Factories.BaseUnit;
using Presentation.Factories.FactorUnit;
using Service.BaseUnit;
using Service.ConvertUnit;
using Service.FactorUnit;
using Service.MeasureDimension;
using Service.StringToMath;

namespace Presentation.Infrastructure
{
    public static class DependencyRegistrar
    {
        public static IServiceCollection Register(this IServiceCollection services)
        {
            services.AddTransient(typeof(IDbContext), typeof(ApplicationDbContext));
            services.AddTransient(typeof(IRepository<>), typeof(EfRepository<>));

            services.AddTransient(typeof(IMeasureDimensionService), typeof(MeasureDimensionService));

            services.AddTransient(typeof(IBaseUnitService), typeof(BaseUnitService));
            services.AddTransient(typeof(IBaseUnitFactory), typeof(BaseUnitFactory));

            services.AddTransient(typeof(IFactorUnitService), typeof(FactorUnitService));
            services.AddTransient(typeof(IFactorUnitFactory), typeof(FactorUnitFactory));

            services.AddTransient(typeof(IConvertUnitService), typeof(ConvertUnitService));

            services.AddTransient(typeof(IStringToMathService), typeof(StringToMathService));

            return services;
        }
    }
}

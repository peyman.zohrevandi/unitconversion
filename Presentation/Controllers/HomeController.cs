﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;
using Core.Domain;
using Core.Dots.ConvertUnit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Presentation.Factories.BaseUnit;
using Presentation.Factories.FactorUnit;
using Presentation.Factories.FormulaUnit;
using Presentation.Models;
using Presentation.ViewModels.BaseUnit;
using Presentation.ViewModels.ConvertUnit;
using Presentation.ViewModels.FactorUnit;
using Presentation.ViewModels.Formula;
using Service.BaseUnit;
using Service.ConvertUnit;
using Service.FactorUnit;

using Service.MeasureDimension;
using Service.StringToMath;

namespace Presentation.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IMeasureDimensionService _measureDimensionService;

        private readonly IBaseUnitService _baseUnitService;

        private readonly IBaseUnitFactory _baseUnitFactory;

        private readonly IFactorUnitService _factorUnitService;

        private readonly IFactorUnitFactory _factorUnitFactory;

        private readonly IConvertUnitService _convertUnitService;

        private readonly IStringToMathService _stringToMathService;

        public HomeController(ILogger<HomeController> logger, IMeasureDimensionService measureDimensionService, IBaseUnitService baseUnitService, IBaseUnitFactory baseUnitFactory,
            IFactorUnitService factorUnitService, IFactorUnitFactory factorUnitFactory, IConvertUnitService convertUnitService, IStringToMathService stringToMathService)
        {
            _logger = logger;
            _measureDimensionService = measureDimensionService;
            _baseUnitService = baseUnitService;
            _baseUnitFactory = baseUnitFactory;
            _factorUnitService = factorUnitService;
            _factorUnitFactory = factorUnitFactory;

            _convertUnitService = convertUnitService;
            _stringToMathService = stringToMathService;
        }

        public IActionResult Index()
        {
            var model = _measureDimensionService.GetAll();

            ViewBag.list = model;

            return View();
        }

        [HttpPost]
        public IActionResult InsertMeasure(MeasureDimension measureDimension)
        {
            if(measureDimension == null)
                throw new ArgumentNullException(nameof(measureDimension));

            var measure = new MeasureDimension
            {
                Name = measureDimension.Name,
                EnName = measureDimension.EnName
            };

            _measureDimensionService.Insert(measure);

           return RedirectToAction("Index");
        }

        public IActionResult BaseUnit()
        {
            var measureList = _measureDimensionService.GetAll()
                .Select(x => new { x.Id, x.Name }).ToList();

            var model = new BaseUnitViewModel
            {
                Measure = new SelectList(measureList, "Id", "Name"),
            };

            var baseUnitList = _baseUnitService.GetAll();

            ViewBag.list = baseUnitList;

            return View(model);
        }

        [HttpPost]
        public IActionResult AddBaseUnit(BaseUnitViewModel baseUnit)
        {
            if(baseUnit == null)
                throw new ArgumentNullException(nameof(baseUnit));

            
            _baseUnitService.Insert(_baseUnitFactory.Create(baseUnit));

            return RedirectToAction("BaseUnit");
        }

        public IActionResult FactorUnit()
        {
            var baseUnitService = _baseUnitService.GetAll()
                .Select(x => new { x.Id, x.Name }).ToList();

            var model = new FactoryUnitViewModel
            {
                BaseUnit = new SelectList(baseUnitService, "Id", "Name"),
            };

            var unitList = _factorUnitService.GetAll();

            ViewBag.list = unitList;

            return View(model);
        }

        [HttpPost]
        public IActionResult AddFactorUnit(FactoryUnitViewModel factorUnit)
        {
            if (factorUnit == null)
                throw new ArgumentNullException(nameof(factorUnit));

            _factorUnitService.Insert(_factorUnitFactory.Create(factorUnit));
           
            return RedirectToAction("FactorUnit");
        }

        public IActionResult Formula()
        {
            return View();
        }

        //[HttpPost]
        //public IActionResult AddFormula(FormulaViewModel formula)
        //{
        //    if (formula == null)
        //        throw new ArgumentNullException(nameof(formula));

        //    _formulaUnitService.Insert(_formulaUnitFactory.Create(formula));

        //    return RedirectToAction("Formula");
        //}

        public IActionResult Category()
        {
            var model = _baseUnitService.GetAll();
           
            return View(model);
        }

        public IActionResult Convert(int baseUnitId)
        
        {
            var factorUnitList = _factorUnitService.GetAll().Where(x=>x.BaseUnitId == baseUnitId)
                .Select(x => new { x.Symbol, x.Name }).ToList();

            var addBaseUnit = _baseUnitService.GetAll().Where(x=>x.Id == baseUnitId)
                .Select(x => new { x.Symbol, x.Name }).FirstOrDefault();

            factorUnitList.Add(addBaseUnit);

            var model = new ConvertUnitViewModel
            {
                FromBaseUnit = new SelectList(factorUnitList, "Symbol", "Name"),
                ToBaseUnit = new SelectList(factorUnitList, "Symbol", "Name")
            };

            
            return View(model);
        }

        [HttpPost]
        public IActionResult Convert(ConvertUnitViewModel model)
        {
            var convert = new ConvertUnitDto
            {
                FromBaseUnitId = model.FromBaseUnitId,
                ToBaseUnitId = model.ToBaseUnitId,
                Number = model.Number,
                BaseUnitId = model.BaseUnitId,
                FromSymbol = model.FromSymbol,
                ToSymbol = model.ToSymbol
            };

        
            var result = _convertUnitService.ConvertUnit(convert);


            TempData["UnitConvert"] = result + " :نتیجه تبدیل";  //model.FromBaseUnit +  model.Number; 

            return RedirectToAction("UnitConvertResult");

        }

        public IActionResult UnitConvertResult()
        {
           var model = TempData["UnitConvert"] as string;

           ViewBag.UnitConvert = model;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}

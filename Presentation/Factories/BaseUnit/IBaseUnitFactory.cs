﻿using Presentation.ViewModels.BaseUnit;

namespace Presentation.Factories.BaseUnit
{
    public interface IBaseUnitFactory
    {
        Core.Domain.BaseUnit Create(BaseUnitViewModel model);
    }
}

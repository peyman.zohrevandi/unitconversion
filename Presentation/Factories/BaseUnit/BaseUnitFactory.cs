﻿using Presentation.ViewModels.BaseUnit;

namespace Presentation.Factories.BaseUnit
{
    public class BaseUnitFactory : IBaseUnitFactory
    {
        public Core.Domain.BaseUnit Create(BaseUnitViewModel model)
        {
      
            return new Core.Domain.BaseUnit
            {
                Name = model.Name,
                EnName = model.EnName,
                MeasureDimensionId = model.MeasureDimensionId,
                Symbol = model.Symbol,
                IsFormula = model.IsFormoula
            };
        }
    }
}

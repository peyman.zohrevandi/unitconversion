﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Presentation.ViewModels.FactorUnit;

namespace Presentation.Factories.FactorUnit
{
    public interface IFactorUnitFactory
    {
        Core.Domain.Unit Create(FactoryUnitViewModel model);
    }
}

﻿using System;
using System.Linq;
using Presentation.ViewModels.FactorUnit;

namespace Presentation.Factories.FactorUnit
{
    public class FactorUnitFactory : IFactorUnitFactory
    {
        public Core.Domain.Unit Create(FactoryUnitViewModel model)
        {
            CheckChar(model);

            return new Core.Domain.Unit
            {
                BaseUnitId = model.BaseUnitId,
                EnName = model.EnName,
                Name = model.Name,
                NumberOfBaseUnit = model.NumberOfBaseUnit,
                Symbol = model.Symbol,
                FromBaseUnitFormula = model.FromBaseUnitFormula,
                ToBaseUnitFormula = model.ToBaseUnitFormula
            };
        }

        public bool CheckChar(FactoryUnitViewModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.FromBaseUnitFormula))
            {
                if (model.FromBaseUnitFormula.Replace(" ", string.Empty).Where(char.IsLetter).Any(c => c != 'a'))
                {
                    throw new Exception("Only a variable is valid");
                }
            }

            if (!string.IsNullOrWhiteSpace(model.ToBaseUnitFormula))
            {
                if (model.ToBaseUnitFormula.Replace(" ", string.Empty).Where(char.IsLetter).Any(c => c != 'a'))
                {
                    throw new Exception("Only a variable is valid");
                }
            }


            return true;
        }
    }
}

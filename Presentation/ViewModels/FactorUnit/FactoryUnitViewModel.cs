﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Presentation.ViewModels.FactorUnit
{
    public class FactoryUnitViewModel
    {
        public string Name { get; set; }

        public string EnName { get; set; }

        public string Symbol { get; set; }

        public double NumberOfBaseUnit { get; set; }

        public int BaseUnitId { get; set; }

        public string FromBaseUnitFormula { get; set; }

        public string ToBaseUnitFormula { get; set; }

        public SelectList BaseUnit { get; set; }
    }
}

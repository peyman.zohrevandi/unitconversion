﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Presentation.ViewModels.BaseUnit
{
    public class BaseUnitViewModel
    {
        public string Name { get; set; }

        public string EnName { get; set; }

        public string Symbol { get; set; }

        public int MeasureDimensionId { get; set; }

        public bool IsFormoula { get; set; }

        public SelectList Measure { get; set; }
    }
}

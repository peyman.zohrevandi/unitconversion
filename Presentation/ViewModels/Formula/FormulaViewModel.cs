﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ViewModels.Formula
{
    public class FormulaViewModel
    {
        public string Name { get; set; }

        public string EnName { get; set; }

        public string Symbol { get; set; }

        public string FromBaseUnit { get; set; }

        public string ToBaseUnit { get; set; }
    }
}

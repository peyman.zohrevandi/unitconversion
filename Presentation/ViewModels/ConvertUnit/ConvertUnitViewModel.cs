﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Presentation.ViewModels.ConvertUnit
{
    public class ConvertUnitViewModel
    {
        public int BaseUnitId { get; set; }

        public int FromBaseUnitId { get; set; }

        public string FromSymbol { get; set; }

        public SelectList FromBaseUnit { get; set; }

        public int ToBaseUnitId { get; set; }

        public string ToSymbol { get; set; }

        public SelectList ToBaseUnit { get; set; }

        public double Number { get; set; }
    }
}

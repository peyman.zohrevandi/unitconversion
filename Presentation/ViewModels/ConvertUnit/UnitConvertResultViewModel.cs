﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Presentation.ViewModels.ConvertUnit
{
    public class UnitConvertResultViewModel
    {
        public double Number { get; set; }

        public int BaseUnitId { get; set; }
    }
}

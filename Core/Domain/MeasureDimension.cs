﻿using System.Collections.Generic;
using Core.BaseClasses;

namespace Core.Domain
{
    public class MeasureDimension : BaseEntity
    {
        public string Name { get; set; }

        public string EnName { get; set; }

        public virtual ICollection<BaseUnit> BaseUnits { get; set; }
    }
}

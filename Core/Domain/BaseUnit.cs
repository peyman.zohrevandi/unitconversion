﻿using System.Collections.Generic;
using Core.BaseClasses;

namespace Core.Domain
{
    public class BaseUnit : BaseEntity
    {
        public string Name { get; set; }

        public string EnName { get; set; }

        public string Symbol { get; set; }

        public virtual int MeasureDimensionId { get; set; }
        public virtual MeasureDimension MeasureDimension { get; set; }

        public bool IsFormula { get; set; }

        public virtual ICollection<Unit> FactorUnits { get; set; }
    }
}

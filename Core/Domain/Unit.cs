﻿using Core.BaseClasses;

namespace Core.Domain
{
    public class Unit : BaseEntity
    {
        public string Name { get; set; }

        public string EnName { get; set; }

        public string Symbol { get; set; }

        public double NumberOfBaseUnit { get; set; }

        public string FromBaseUnitFormula { get; set; }

        public string ToBaseUnitFormula { get; set; }

        public int BaseUnitId { get; set; }
        public BaseUnit BaseUnit { get; set; }
    }
}

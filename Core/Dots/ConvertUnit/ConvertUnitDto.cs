﻿namespace Core.Dots.ConvertUnit
{
    public class ConvertUnitDto
    {
        public int FromBaseUnitId { get; set; }

        public string FromSymbol { get; set; }

        public int ToBaseUnitId { get; set; }

        public string ToSymbol { get; set; }

        public double Number { get; set; }

        public int BaseUnitId { get; set; }
    }
}

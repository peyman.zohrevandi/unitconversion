﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [FormulaUnits] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [FromBaseUnit] nvarchar(max) NULL,
    [ToBaseUnit] nvarchar(max) NULL,
    CONSTRAINT [PK_FormulaUnits] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [MeasureDimensions] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    CONSTRAINT [PK_MeasureDimensions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [BaseUnits] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [MeasureDimensionId] int NOT NULL,
    CONSTRAINT [PK_BaseUnits] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_BaseUnits_MeasureDimensions_MeasureDimensionId] FOREIGN KEY ([MeasureDimensionId]) REFERENCES [MeasureDimensions] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [FactorUnits] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [NumberOfBaseUnit] float NOT NULL,
    [BaseUnitId] int NOT NULL,
    CONSTRAINT [PK_FactorUnits] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_FactorUnits_BaseUnits_BaseUnitId] FOREIGN KEY ([BaseUnitId]) REFERENCES [BaseUnits] ([Id]) ON DELETE CASCADE
);

GO

CREATE INDEX [IX_BaseUnits_MeasureDimensionId] ON [BaseUnits] ([MeasureDimensionId]);

GO

CREATE INDEX [IX_FactorUnits_BaseUnitId] ON [FactorUnits] ([BaseUnitId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200714152704_Init', N'3.1.6');

GO

ALTER TABLE [FactorUnits] ADD [FromBaseUnit] nvarchar(max) NULL;

GO

ALTER TABLE [FactorUnits] ADD [IsFormula] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

ALTER TABLE [FactorUnits] ADD [ToBaseUnit] nvarchar(max) NULL;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200717171340_Change FactorUnit', N'3.1.6');

GO

DROP TABLE [FormulaUnits];

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200717181614_Remove FactorUnits', N'3.1.6');

GO

DROP TABLE [FactorUnits];

GO

ALTER TABLE [BaseUnits] ADD [IsFormula] bit NOT NULL DEFAULT CAST(0 AS bit);

GO

CREATE TABLE [Units] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [NumberOfBaseUnit] float NOT NULL,
    [FromBaseUnitFormula] nvarchar(max) NULL,
    [ToBaseUnitFormula] nvarchar(max) NULL,
    [BaseUnitId] int NOT NULL,
    CONSTRAINT [PK_Units] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_Units_BaseUnits_BaseUnitId] FOREIGN KEY ([BaseUnitId]) REFERENCES [BaseUnits] ([Id]) ON DELETE CASCADE
);

GO

CREATE INDEX [IX_Units_BaseUnitId] ON [Units] ([BaseUnitId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200718010713_Change Units', N'3.1.6');

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'EnName', N'Name') AND [object_id] = OBJECT_ID(N'[MeasureDimensions]'))
    SET IDENTITY_INSERT [MeasureDimensions] ON;
INSERT INTO [MeasureDimensions] ([Id], [EnName], [Name])
VALUES (1, N'Length', N'طول'),
(2, N'g', N'جرم'),
(3, N'electric current', N'جریان الکتریکی'),
(4, N'S', N'زمان'),
(5, N'D', N'دما');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'EnName', N'Name') AND [object_id] = OBJECT_ID(N'[MeasureDimensions]'))
    SET IDENTITY_INSERT [MeasureDimensions] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'EnName', N'IsFormula', N'MeasureDimensionId', N'Name', N'Symbol') AND [object_id] = OBJECT_ID(N'[BaseUnits]'))
    SET IDENTITY_INSERT [BaseUnits] ON;
INSERT INTO [BaseUnits] ([Id], [EnName], [IsFormula], [MeasureDimensionId], [Name], [Symbol])
VALUES (1, N'Meter', CAST(0 AS bit), 1, N'متر', N'm'),
(2, N'Gram', CAST(0 AS bit), 2, N'گرم', N'g'),
(3, N'Ampere', CAST(0 AS bit), 3, N'آمپر', N'A'),
(4, N'Second', CAST(0 AS bit), 4, N'ثانیه', N'S'),
(5, N'Celsius', CAST(1 AS bit), 5, N'سلسیوس', N'C');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'EnName', N'IsFormula', N'MeasureDimensionId', N'Name', N'Symbol') AND [object_id] = OBJECT_ID(N'[BaseUnits]'))
    SET IDENTITY_INSERT [BaseUnits] OFF;

GO

IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'BaseUnitId', N'EnName', N'FromBaseUnitFormula', N'Name', N'NumberOfBaseUnit', N'Symbol', N'ToBaseUnitFormula') AND [object_id] = OBJECT_ID(N'[Units]'))
    SET IDENTITY_INSERT [Units] ON;
INSERT INTO [Units] ([Id], [BaseUnitId], [EnName], [FromBaseUnitFormula], [Name], [NumberOfBaseUnit], [Symbol], [ToBaseUnitFormula])
VALUES (1, 1, N'Millimeter', NULL, N'میلی متر', 0.001E0, N'mm', NULL),
(2, 1, N'Centimeter', NULL, N'سانتی متر', 0.01E0, N'cm', NULL),
(3, 1, N'Kilometer', NULL, N'کیلومتر', 1000.0E0, N'km', NULL),
(4, 2, N'Milligram', NULL, N'میلی گرم', 0.001E0, N'mg', NULL),
(5, 2, N'Kilogram', NULL, N'کیلو گرم', 1000.0E0, N'kg', NULL),
(6, 2, N'Tonne', NULL, N'تن', 1000000.0E0, N'ton', NULL),
(7, 5, N'Kelvin', N'a + 273.15', N'کلوین', 0.0E0, N'K', N'a - 273.15'),
(8, 5, N'Fahrenheit', N'(a * 9/5) + 32', N'فارنهایت', 0.0E0, N'F', N'(a - 32) * 5/9');
IF EXISTS (SELECT * FROM [sys].[identity_columns] WHERE [name] IN (N'Id', N'BaseUnitId', N'EnName', N'FromBaseUnitFormula', N'Name', N'NumberOfBaseUnit', N'Symbol', N'ToBaseUnitFormula') AND [object_id] = OBJECT_ID(N'[Units]'))
    SET IDENTITY_INSERT [Units] OFF;

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200718213315_Seed ', N'3.1.6');

GO


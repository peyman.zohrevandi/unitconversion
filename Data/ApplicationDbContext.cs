﻿using System;
using System.Linq;
using Core.BaseClasses;
using Core.Domain;
using Microsoft.EntityFrameworkCore;

namespace Data
{
    public class ApplicationDbContext : DbContext, IDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<MeasureDimension> MeasureDimensions { get; set; }

        public DbSet<BaseUnit> BaseUnits { get; set; }

        public DbSet<Unit> Units { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Seed();
        }


        #region Methods

        /// <summary>
        /// Creates a DbSet that can be used to query and save instances of entity
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <returns>A set for the given entity type</returns>
        public new virtual DbSet<TEntity> Set<TEntity>() where TEntity : BaseEntity
        {
            return base.Set<TEntity>();
        }

        /// <summary>
        /// Generate a script to create all tables for the current model
        /// </summary>
        /// <returns>A SQL script</returns>
        public virtual string GenerateCreateScript()
        {
            return Database.GenerateCreateScript();
        }

        /// <summary>
        /// Creates a LINQ query for the query type based on a raw SQL query
        /// </summary>
        /// <typeparam name="TQuery">Query type</typeparam>
        /// <param name="sql">The raw SQL query</param>
        /// <param name="parameters">The values to be assigned to parameters</param>
        /// <returns>An IQueryable representing the raw SQL query</returns>
        public virtual IQueryable<TQuery> QueryFromSql<TQuery>(string sql, params object[] parameters) where TQuery : class
        {
            //return Query<TQuery>().FromSql(CreateSqlWithParameters(sql, parameters), parameters);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a LINQ query for the entity based on a raw SQL query
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="sql">The raw SQL query</param>
        /// <param name="parameters">The values to be assigned to parameters</param>
        /// <returns>An IQueryable representing the raw SQL query</returns>
        public virtual IQueryable<TEntity> EntityFromSql<TEntity>(string sql, params object[] parameters) where TEntity : BaseEntity
        {
            //return Set<TEntity>().FromSql(CreateSqlWithParameters(sql, parameters), parameters);
            throw new NotImplementedException();
        }

        /// <summary>
        /// Executes the given SQL against the database
        /// </summary>
        /// <param name="sql">The SQL to execute</param>
        /// <param name="doNotEnsureTransaction">true - the transaction creation is not ensured; false - the transaction creation is ensured.</param>
        /// <param name="timeout">The timeout to use for command. Note that the command timeout is distinct from the connection timeout, which is commonly set on the database connection string</param>
        /// <param name="parameters">Parameters to use with the SQL</param>
        /// <returns>The number of rows affected</returns>
        public virtual int ExecuteSqlCommand(RawSqlString sql, bool doNotEnsureTransaction = false, int? timeout = null, params object[] parameters)
        {
            //set specific command timeout
            var previousTimeout = Database.GetCommandTimeout();
            Database.SetCommandTimeout(timeout);

            var result = 0;
            if (!doNotEnsureTransaction)
            {
                //use with transaction
                using (var transaction = Database.BeginTransaction())
                {
                    result = Database.ExecuteSqlCommand(sql, parameters);
                    transaction.Commit();
                }
            }
            else
                result = Database.ExecuteSqlCommand(sql, parameters);

            //return previous timeout back
            Database.SetCommandTimeout(previousTimeout);

            return result;
        }

        /// <summary>
        /// Detach an entity from the context
        /// </summary>
        /// <typeparam name="TEntity">Entity type</typeparam>
        /// <param name="entity">Entity</param>
        public virtual void Detach<TEntity>(TEntity entity) where TEntity : BaseEntity
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            var entityEntry = Entry(entity);
            if (entityEntry == null)
                return;

            //set the entity is not being tracked by the context
            entityEntry.State = EntityState.Detached;
        }

        #endregion
    }

    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MeasureDimension>().HasData(
                new MeasureDimension { Id = 1,EnName = "Length", Name = "طول" },
                new MeasureDimension { Id = 2, EnName = "g", Name = "جرم" },
                new MeasureDimension { Id = 3, EnName = "electric current", Name = "جریان الکتریکی" },
                new MeasureDimension { Id = 4, EnName = "S", Name = "زمان" },
                new MeasureDimension { Id = 5, EnName = "D", Name = "دما" }
            );

            modelBuilder.Entity<BaseUnit>().HasData(
                new BaseUnit { Id = 1,EnName = "Meter", Name = "متر",Symbol = "m", MeasureDimensionId = 1,IsFormula = false},
                new BaseUnit { Id = 2, EnName = "Gram", Name = "گرم",Symbol = "g", MeasureDimensionId = 2, IsFormula = false },
                new BaseUnit { Id = 3, EnName = "Ampere", Name = "آمپر",Symbol = "A", MeasureDimensionId = 3, IsFormula = false },
                new BaseUnit { Id = 4, EnName = "Second", Name = "ثانیه",Symbol = "S", MeasureDimensionId = 4, IsFormula = false },
                new BaseUnit { Id = 5, EnName = "Celsius", Name = "سلسیوس",Symbol = "C", MeasureDimensionId = 5, IsFormula = true }
            );

           
            modelBuilder.Entity<Unit>().HasData(
                new Unit { Id = 1, EnName = "Millimeter", Name = "میلی متر", Symbol = "mm", NumberOfBaseUnit = 0.001, FromBaseUnitFormula = null,ToBaseUnitFormula = null, BaseUnitId = 1},
                new Unit { Id = 2, EnName = "Centimeter", Name = "سانتی متر", Symbol = "cm", NumberOfBaseUnit = 0.01, FromBaseUnitFormula = null, ToBaseUnitFormula = null, BaseUnitId = 1 },
                new Unit { Id = 3, EnName = "Kilometer", Name = "کیلومتر", Symbol = "km", NumberOfBaseUnit = 1000, FromBaseUnitFormula = null, ToBaseUnitFormula = null, BaseUnitId = 1 },

                new Unit { Id = 4, EnName = "Milligram", Name = "میلی گرم", Symbol = "mg", NumberOfBaseUnit = 0.001, FromBaseUnitFormula = null, ToBaseUnitFormula = null, BaseUnitId = 2 },
                new Unit { Id = 5, EnName = "Kilogram", Name = "کیلو گرم", Symbol = "kg", NumberOfBaseUnit = 1000, FromBaseUnitFormula = null, ToBaseUnitFormula = null, BaseUnitId = 2 },
                new Unit { Id = 6, EnName = "Tonne", Name = "تن", Symbol = "ton", NumberOfBaseUnit = 1000000, FromBaseUnitFormula = null, ToBaseUnitFormula = null, BaseUnitId = 2 },

                new Unit { Id = 7, EnName = "Kelvin", Name = "کلوین", Symbol = "K", NumberOfBaseUnit = 0, FromBaseUnitFormula = "a + 273.15", ToBaseUnitFormula = "a - 273.15", BaseUnitId = 5 },
                new Unit { Id = 8, EnName = "Fahrenheit", Name = "فارنهایت", Symbol = "F", NumberOfBaseUnit = 0, FromBaseUnitFormula = "(a * 9/5) + 32", ToBaseUnitFormula = "(a - 32) * 5/9", BaseUnitId = 5 }
            );
        }
    }
}

﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [FormulaUnits] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [FromBaseUnit] nvarchar(max) NULL,
    [ToBaseUnit] nvarchar(max) NULL,
    CONSTRAINT [PK_FormulaUnits] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [MeasureDimensions] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    CONSTRAINT [PK_MeasureDimensions] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [BaseUnits] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [MeasureDimensionId] int NOT NULL,
    CONSTRAINT [PK_BaseUnits] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_BaseUnits_MeasureDimensions_MeasureDimensionId] FOREIGN KEY ([MeasureDimensionId]) REFERENCES [MeasureDimensions] ([Id]) ON DELETE CASCADE
);

GO

CREATE TABLE [FactorUnits] (
    [Id] int NOT NULL IDENTITY,
    [Name] nvarchar(max) NULL,
    [EnName] nvarchar(max) NULL,
    [Symbol] nvarchar(max) NULL,
    [NumberOfBaseUnit] float NOT NULL,
    [BaseUnitId] int NOT NULL,
    CONSTRAINT [PK_FactorUnits] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_FactorUnits_BaseUnits_BaseUnitId] FOREIGN KEY ([BaseUnitId]) REFERENCES [BaseUnits] ([Id]) ON DELETE CASCADE
);

GO

CREATE INDEX [IX_BaseUnits_MeasureDimensionId] ON [BaseUnits] ([MeasureDimensionId]);

GO

CREATE INDEX [IX_FactorUnits_BaseUnitId] ON [FactorUnits] ([BaseUnitId]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20200714152704_Init', N'3.1.6');

GO


﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class ChangeUnits : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FactorUnits");

            migrationBuilder.AddColumn<bool>(
                name: "IsFormula",
                table: "BaseUnits",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "Units",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EnName = table.Column<string>(nullable: true),
                    Symbol = table.Column<string>(nullable: true),
                    NumberOfBaseUnit = table.Column<double>(nullable: false),
                    FromBaseUnitFormula = table.Column<string>(nullable: true),
                    ToBaseUnitFormula = table.Column<string>(nullable: true),
                    BaseUnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Units", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Units_BaseUnits_BaseUnitId",
                        column: x => x.BaseUnitId,
                        principalTable: "BaseUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Units_BaseUnitId",
                table: "Units",
                column: "BaseUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Units");

            migrationBuilder.DropColumn(
                name: "IsFormula",
                table: "BaseUnits");

            migrationBuilder.CreateTable(
                name: "FactorUnits",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BaseUnitId = table.Column<int>(type: "int", nullable: false),
                    EnName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    FromBaseUnit = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsFormula = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NumberOfBaseUnit = table.Column<double>(type: "float", nullable: false),
                    Symbol = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ToBaseUnit = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactorUnits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FactorUnits_BaseUnits_BaseUnitId",
                        column: x => x.BaseUnitId,
                        principalTable: "BaseUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_FactorUnits_BaseUnitId",
                table: "FactorUnits",
                column: "BaseUnitId");
        }
    }
}

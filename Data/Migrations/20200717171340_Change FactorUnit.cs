﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class ChangeFactorUnit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FromBaseUnit",
                table: "FactorUnits",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsFormula",
                table: "FactorUnits",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "ToBaseUnit",
                table: "FactorUnits",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FromBaseUnit",
                table: "FactorUnits");

            migrationBuilder.DropColumn(
                name: "IsFormula",
                table: "FactorUnits");

            migrationBuilder.DropColumn(
                name: "ToBaseUnit",
                table: "FactorUnits");
        }
    }
}

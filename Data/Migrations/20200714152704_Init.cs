﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "FormulaUnits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EnName = table.Column<string>(nullable: true),
                    Symbol = table.Column<string>(nullable: true),
                    FromBaseUnit = table.Column<string>(nullable: true),
                    ToBaseUnit = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FormulaUnits", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MeasureDimensions",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EnName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MeasureDimensions", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "BaseUnits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EnName = table.Column<string>(nullable: true),
                    Symbol = table.Column<string>(nullable: true),
                    MeasureDimensionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BaseUnits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_BaseUnits_MeasureDimensions_MeasureDimensionId",
                        column: x => x.MeasureDimensionId,
                        principalTable: "MeasureDimensions",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "FactorUnits",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    EnName = table.Column<string>(nullable: true),
                    Symbol = table.Column<string>(nullable: true),
                    NumberOfBaseUnit = table.Column<double>(nullable: false),
                    BaseUnitId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FactorUnits", x => x.Id);
                    table.ForeignKey(
                        name: "FK_FactorUnits_BaseUnits_BaseUnitId",
                        column: x => x.BaseUnitId,
                        principalTable: "BaseUnits",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BaseUnits_MeasureDimensionId",
                table: "BaseUnits",
                column: "MeasureDimensionId");

            migrationBuilder.CreateIndex(
                name: "IX_FactorUnits_BaseUnitId",
                table: "FactorUnits",
                column: "BaseUnitId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "FactorUnits");

            migrationBuilder.DropTable(
                name: "FormulaUnits");

            migrationBuilder.DropTable(
                name: "BaseUnits");

            migrationBuilder.DropTable(
                name: "MeasureDimensions");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class Seed : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "MeasureDimensions",
                columns: new[] { "Id", "EnName", "Name" },
                values: new object[,]
                {
                    { 1, "Length", "طول" },
                    { 2, "g", "جرم" },
                    { 3, "electric current", "جریان الکتریکی" },
                    { 4, "S", "زمان" },
                    { 5, "D", "دما" }
                });

            migrationBuilder.InsertData(
                table: "BaseUnits",
                columns: new[] { "Id", "EnName", "IsFormula", "MeasureDimensionId", "Name", "Symbol" },
                values: new object[,]
                {
                    { 1, "Meter", false, 1, "متر", "m" },
                    { 2, "Gram", false, 2, "گرم", "g" },
                    { 3, "Ampere", false, 3, "آمپر", "A" },
                    { 4, "Second", false, 4, "ثانیه", "S" },
                    { 5, "Celsius", true, 5, "سلسیوس", "C" }
                });

            migrationBuilder.InsertData(
                table: "Units",
                columns: new[] { "Id", "BaseUnitId", "EnName", "FromBaseUnitFormula", "Name", "NumberOfBaseUnit", "Symbol", "ToBaseUnitFormula" },
                values: new object[,]
                {
                    { 1, 1, "Millimeter", null, "میلی متر", 0.001, "mm", null },
                    { 2, 1, "Centimeter", null, "سانتی متر", 0.01, "cm", null },
                    { 3, 1, "Kilometer", null, "کیلومتر", 1000.0, "km", null },
                    { 4, 2, "Milligram", null, "میلی گرم", 0.001, "mg", null },
                    { 5, 2, "Kilogram", null, "کیلو گرم", 1000.0, "kg", null },
                    { 6, 2, "Tonne", null, "تن", 1000000.0, "ton", null },
                    { 7, 5, "Kelvin", "a + 273.15", "کلوین", 0.0, "K", "a - 273.15" },
                    { 8, 5, "Fahrenheit", "(a * 9/5) + 32", "فارنهایت", 0.0, "F", "(a - 32) * 5/9" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BaseUnits",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "BaseUnits",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "BaseUnits",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "BaseUnits",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "BaseUnits",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "MeasureDimensions",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "MeasureDimensions",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "MeasureDimensions",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "MeasureDimensions",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "MeasureDimensions",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
